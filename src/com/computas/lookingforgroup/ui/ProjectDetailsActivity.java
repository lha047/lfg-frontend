package com.computas.lookingforgroup.ui;

import android.support.v4.app.Fragment;

public class ProjectDetailsActivity extends SimpleSinglePaneActivity{

	@Override
	protected Fragment onCreatePane() {
		return new ProjectDetailsFragment();
	}

}
