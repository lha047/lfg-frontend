package com.computas.lookingforgroup.model;

import java.util.List;

public class Project {
	
	public String id;
	
	public String name;
	
	public String description;
	
	public List<Profile> members;

	public List<Skill> requestedSkills;
	
	@Override
	public String toString() {
		return name;
	}
}
