package com.computas.lookingforgroup.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.computas.lookingforgroup.Constants;
import com.computas.lookingforgroup.R;
import com.computas.lookingforgroup.model.Profile;

public class ProfilesListFragment extends ListFragment{
	private ProfilesAdapter mProfilesAdapter;
	private List<Profile> mProfilesList = new ArrayList<Profile>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Profile g1 = new Profile();
		g1.id = "1";
		g1.name = "Frederik Skytte";
		Profile g2 = new Profile();
		g2.id = "1";
		g2.name = "Anders And";
		Profile g3 = new Profile();
		g3.id = "1";
		g3.name = "Bjarne Bjarmes";
		
		mProfilesList.add(g1);
		mProfilesList.add(g2);
		mProfilesList.add(g3);
		
		mProfilesAdapter = new ProfilesAdapter(getActivity());
	
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getListView().setBackgroundColor(Color.BLACK);
		setListAdapter(mProfilesAdapter);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		//Load data for the list here
		new GetProfilesTask().execute("profiles/");
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		//Show details for the clicked profile
		
		//Intent intent = new Intent(getActivity(), ????.class);
		//intent.setData(Uri.parse("profiles/" + mProfilesList.get(position).id));
		//startActivity(intent);
	}
	
	private class ProfilesAdapter extends ArrayAdapter<Profile> {

		public ProfilesAdapter(Context context) {
			super(context, R.layout.list_item_group, R.id.group_name, mProfilesList);
		}
	}
	
	private class GetProfilesTask extends AsyncTask<String, Void, Profile[]> {

		@Override
		protected Profile[] doInBackground(String... params) {
			try {
				String url = Constants.BASE_URL + params[0];
				
				// Create a new RestTemplate instance
				RestTemplate restTemplate = new RestTemplate();
				// Add the Gson message converter
				restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
				
				Profile[] profiles = restTemplate.getForObject(url, Profile[].class);

				return profiles;
			} catch (Exception e) {
				Log.e("RestCall", "Get profiles failed: " + e.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Profile[] profiles) {
			if (profiles != null) {
				Log.e("RestCall", "Got: " + profiles.length);
				mProfilesList.clear();
				mProfilesList.addAll(Arrays.asList(profiles));
				mProfilesAdapter.notifyDataSetChanged();
			}
		}
	}
}