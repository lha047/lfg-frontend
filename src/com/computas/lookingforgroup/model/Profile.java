package com.computas.lookingforgroup.model;

import java.util.List;

public class Profile {
	
	public String id;
	
	public String name;
	
	public String programme;
	
	public String description;
	
	public transient List<Project> projects;
	
	public transient List<Skill> skills;

	@Override
	public String toString() {
		return name;
	}
}
